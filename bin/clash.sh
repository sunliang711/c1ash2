#!/bin/bash
if [ -z "${BASH_SOURCE}" ]; then
    this=${PWD}
else
    rpath="$(readlink ${BASH_SOURCE})"
    if [ -z "$rpath" ]; then
        rpath=${BASH_SOURCE}
    fi
    this="$(cd $(dirname $rpath) && pwd)"
fi

export PATH=$PATH:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

user="${SUDO_USER:-$(whoami)}"
home="$(eval echo ~$user)"

# export TERM=xterm-256color

# Use colors, but only if connected to a terminal, and that terminal
# supports them.
if which tput >/dev/null 2>&1; then
  ncolors=$(tput colors 2>/dev/null)
fi
if [ -t 1 ] && [ -n "$ncolors" ] && [ "$ncolors" -ge 8 ]; then
    RED="$(tput setaf 1)"
    GREEN="$(tput setaf 2)"
    YELLOW="$(tput setaf 3)"
    BLUE="$(tput setaf 4)"
    CYAN="$(tput setaf 5)"
    BOLD="$(tput bold)"
    NORMAL="$(tput sgr0)"
else
    RED=""
    GREEN=""
    YELLOW=""
    CYAN=""
    BLUE=""
    BOLD=""
    NORMAL=""
fi

_onlyLinux(){
    if [ $(uname) != "Linux" ];then
        _err "Only on linux"
        exit 1
    fi
}

_err(){
    echo "$*" >&2
}

_command_exists(){
    command -v "$@" > /dev/null 2>&1
}

rootID=0

_runAsRoot(){
    cmd="${*}"
    bash_c='bash -c'
    if [ "${EUID}" -ne "${rootID}" ];then
        if _command_exists sudo; then
            bash_c='sudo -E bash -c'
        elif _command_exists su; then
            bash_c='su -c'
        else
            cat >&2 <<-'EOF'
			Error: this installer needs the ability to run commands as root.
			We are unable to find either "sudo" or "su" available to make this happen.
			EOF
            exit 1
        fi
    fi
    # only output stderr
    (set -x; $bash_c "${cmd}")
}

function _insert_path(){
    if [ -z "$1" ];then
        return
    fi
    echo -e ${PATH//:/"\n"} | grep -c "^$1$" >/dev/null 2>&1 || export PATH=$1:$PATH
}

_run(){
    # only output stderr
    cmd="$*"
    (set -x; bash -c "${cmd}")
}

function _root(){
    if [ ${EUID} -ne ${rootID} ];then
        echo "Need run as root!"
        echo "Requires root privileges."
        exit 1
    fi
}

ed=vi
if _command_exists vim; then
    ed=vim
fi
if _command_exists nvim; then
    ed=nvim
fi
# use ENV: editor to override
if [ -n "${editor}" ];then
    ed=${editor}
fi
###############################################################################
# write your code below (just define function[s])
# function is hidden when begin with '_'
###############################################################################
binName=clash
root="$(cd ${this}/.. && pwd)"
etcDir="${root}/etc"
templateDir="${root}/template"
gatewayFile="${etcDir}/gateway.txt"

case $(uname) in
    Linux)
        cmdStat=stat
        ;;
    Darwin)
        cmdStat='stat -x'
        ;;
esac
# logfile=/tmp/clash.log
# configFile=${this}/../config.yaml
# configExampleFile=${this}/../config-example.yaml

_createServiceFileOnDemand(){
    local name=${1:?'missing config name(no extension)'}

    local subDir="${root}/etc/${name}"
    # link Country.mmdb
    (cd ${subDir} && _run "ln -sf ../../Country.mmdb .")

    # service file
    case $(uname) in
        Darwin)
            # if [ -e  $home/Library/LaunchAgents/clash-${name}.plist ];then
            #     return
            # fi
            sed -e "s|<NAME>|${name}|g" \
                -e "s|<CWD>|${subDir}|g" \
                -e "s|<EXE>|${root}/clash|g" \
                -e "s|<CONFIG>|${name}.yaml|g" \
                ${templateDir}/clash.plist > $home/Library/LaunchAgents/clash-${name}.plist
        ;;
        Linux)
            # if [ -e /etc/systemd/system/clash-${name}.service ];then
            #     return
            # fi
            local start_pre="${this}/clash.sh _start_pre ${name}"
            local start="${root}/clash -d . -f ${name}.yaml"
            local start_post="${this}/clash.sh _start_post ${name}"
            local stop_post="${this}/clash.sh _stop_post ${name}"
            sed -e "s|<START_PRE>|${start_pre}|g" \
                -e "s|<START>|${start}|g" \
                -e "s|<START_POST>|${start_post}|g" \
                -e "s|<STOP_POST>|${stop_post}|g" \
                -e "s|<USER>|root|g" \
                -e "s|<CWD>|${subDir}|g" \
                ${templateDir}/clash.service > /tmp/clash-${name}.service
            _runAsRoot "mv /tmp/clash-${name}.service /etc/systemd/system"
            _runAsRoot "systemctl daemon-reload"
            _runAsRoot "systemctl enable clash-${name}.service"
        ;;
    esac
}

add(){
    local name=${1:?'missing config name(no extension)'}
    name=${name%.json}

    # exist?
    if [ -e ${etcDir}/$name ];then
        echo "${RED}Error: already exists${NORMAL}"
        exit 1
    fi

    set -e

    # create a subDir for this one
    local subDir="${root}/etc/${name}"
    if [ ! -d ${subDir} ];then
        mkdir -p ${subDir}
    fi

    # copy a config file
    (
        cd ${subDir}
        if [ ! -e ${name}.yaml ];then
            cp ${templateDir}/config-example.yaml ${name}.yaml
        fi
    )

    ${ed} ${etcDir}/$name/${name}.yaml

    _createServiceFileOnDemand $name
}

remove(){
    local name=${1:?'missing config name(no extension)'}
    name=${name%.json}

    local subDir="${root}/etc/${name}"

    if [ ! -e ${subDir} ];then
        echo "Error: not found runtime dir: ${subDir}"
        exit 1
    fi

    stop ${name}
    case $(uname) in
        Linux)
            _runAsRoot "/bin/rm -rf /etc/systemd/system/clash-${name}.service"
            _runAsRoot "systemctl daemon-reload"
            ;;
        Darwin)
            _run "/bin/rm -rf $home/Library/LaunchAgents/clash-${name}.plist"
            ;;
    esac

    _run "/bin/rm -rf ${subDir}"
}

_start_pre(){
    echo "_start_pre"
    local name=${1:?'missing config name(no extension)'}
    name=${name%.json}
    if [ ! -e ${root}/clash ];then
        echo "Error: not found clash executable!"
        exit 1
    fi
    local subDir="${root}/etc/${name}"

    if [ ! -e ${subDir} ];then
        echo "Error: not found runtime dir: ${subDir}"
        exit 1
    fi
}

_start_post(){
    echo "_start_post"
    local name=${1:?'missing config name(no extension)'}
    name=${name%.json}
    if [ -e ${gatewayFile} ];then
        local gatewayName="$(cat ${gatewayFile})"
        if [ ${name} = ${gatewayName} ];then
            echo "+ ${name} is gateway,set it..."
            _set ${name}
        fi
    fi

    local subDir="${root}/etc/${name}"
    local configFile="${subDir}/${name}.yaml"
    grep '^mode:' ${configFile} | perl -ne 'print $1 if /^mode:\s+(\w+)/'
    if grep '^mode:' ${configFile} | grep -qi 'global';then
        echo "clash is running on global mode,set GLOBAL group.."
        setAnyProxy ${name} GLOBAL Nexitally
    fi
}

_stop_post(){
    echo "_stop_post"
    local name=${1:?'missing config name(no extension)'}
    name=${name%.json}
    if [ -e ${gatewayFile} ];then
        gatewayName="$(cat ${gatewayFile})"
        if [ ${name} = ${gatewayName} ];then
            echo "+ ${name} is gateway,clear it..."
            _clear ${name}
        fi
    fi
}

start(){
    local name=${1:?'missing config name(no extension)'}
    name=${name%.json}
    local subDir="${root}/etc/${name}"
    local configFile="${subDir}/${name}.yaml"

    if [ ! -e ${subDir} ];then
        echo "Error: not found runtime dir: ${subDir}"
        exit 1
    fi

    _createServiceFileOnDemand $name

    case $(uname) in
        Linux)
            _runAsRoot "systemctl start clash-${name}.service"
            ;;
        Darwin)
            launchctl load -w $home/Library/LaunchAgents/clash-${name}.plist 2>/dev/null
            ;;
    esac

    echo "check status..."
    if status ${name} >/dev/null;then
        port=$(grep '^port:' $configFile | awk '{print $2}')
        if [ -n $port ];then
            if [ -n "${PROXY_FILE}" ];then
                echo "Write http proxy to PROXY_FILE: ${PROXY_FILE}"
                echo "http_proxy=http://localhost:${port}" >${PROXY_FILE}
            fi
        else
            echo "${RED}Error${NORMAL}: get http port error."
        fi

        if [ $(uname) = "Darwin" ];then
            if [ -n $port ];then
                echo "Set system http proxy: localhost:$port"
                echo "Set system https proxy: localhost:$port"
                bash ${root}/setMacProxy.sh http $port >/dev/null
                bash ${root}/setMacProxy.sh https $port >/dev/null
            fi
        fi
        echo "OK: ${name} is running now."
    else
        echo "Error: ${name} is not running."
    fi

}

gw(){
    cat ${gatewayFile}
}

setgw(){
    _onlyLinux
    local name=${1:?'missing config name(no extension)'}
    name=${name%.json}
    local subDir="${root}/etc/${name}"
    if [ ! -e ${subDir} ];then
        echo "Error: not found runtime dir: ${subDir}"
        exit 1
    fi

    echo "Tip: set redir_port in config file!!"
    echo "Tip: only 1 config file set dns true!!"
    echo ${name} > ${gatewayFile}

}

stop(){
    local name=${1:?'missing config name(no extension)'}
    name=${name%.json}
    local subDir="${root}/etc/${name}"

    if [ ! -e ${subDir} ];then
        echo "Error: not found runtime dir: ${subDir}"
        exit 1
    fi

    echo "stop clash ${name}..."
    case $(uname) in
        Linux)
            _runAsRoot systemctl stop clash-${name}.service
            ;;
        Darwin)
            launchctl unload -w $home/Library/LaunchAgents/clash-${name}.plist 2>/dev/null
            bash ${root}/setMacProxy.sh unset
            ;;
    esac
}

# fake-ip 配合tun不需要iptables就可以透明网关
# redir-host 配合tun需要再设置iptables才可以透明网关
# fake-ip 当透明网关带宽太低
_set(){
    # return
    echo "_set"
    local name=${1:?'missing config name(no extension)'}
    name=${name%.json}
    local subDir="${root}/etc/${name}"
    local configFile="${subDir}/${name}.yaml"

    local redir_port="$(perl -lne 'print $1 if /^\s*redir-port:\s*(\d+)/' ${configFile})"
    if [ -z "${redir_port}" ];then
        echo "Cannot find redir_port"
        exit 1
    fi
    echo "${green}Found redir_port: ${redir_port}${reset}"

    local tproxy_port="$(perl -lne 'print $1 if /^\s*tproxy-port:\s*(\d+)/' ${configFile})"
    if [ -z "${tproxy_port}" ];then
        echo "Cannot find tproxy_port"
        exit 1
    fi
    echo "${green}Found tproxy_port: ${tproxy_port}${reset}"
    local defaultDev="$(ip r s | grep default | awk '{print $5}')"
    echo "default dev: ${defaultDev}"
    local subnet="$(ip r s | grep -v default | grep ${defaultDev} | awk '{print $1}')"
    echo "subnet: ${subnet}"

    sysctl -w net.ipv4.ip_forward=1
    iptables -t nat -A POSTROUTING -o ${defaultDev} -j MASQUERADE
    iptables -P FORWARD ACCEPT

    cmd="$(cat<<EOF
    iptables -t mangle -N clash || { iptables -t mangle -F clash; }
    iptables -t mangle -A clash -d 0.0.0.0/8 -j RETURN
    iptables -t mangle -A clash -d 10.0.0.0/8 -j RETURN
    iptables -t mangle -A clash -d 127.0.0.0/8 -j RETURN
    iptables -t mangle -A clash -d 169.254.0.0/16 -j RETURN
    iptables -t mangle -A clash -d 172.16.0.0/12 -j RETURN
    iptables -t mangle -A clash -d 192.168.0.0/16 -j RETURN
    iptables -t mangle -A clash -d 224.0.0.0/4 -j RETURN
    iptables -t mangle -A clash -d 240.0.0.0/4 -j RETURN

    # 解决外网端口映射失效,把所有端口都指定一下
    iptables -t mangle -A clash -p tcp -m multiport --sports 22 -j RETURN
    iptables -t mangle -A clash -p udp -m multiport --sports 51830 -j RETURN
    iptables -t mangle -A clash -p tcp -m multiport --sports 8006 -j RETURN
    iptables -t mangle -A clash -p tcp -m multiport --sports 32400 -j RETURN

    iptables -t mangle -A clash -p tcp -j TPROXY --on-port ${tproxy_port} --tproxy-mark 1
    iptables -t mangle -A clash -p udp -j TPROXY --on-port ${tproxy_port} --tproxy-mark 1
    iptables -t mangle -A PREROUTING -j clash

    ip rule add fwmark 1 table 100 || { echo 'ignore error'; }
    ip route add local default dev lo table 100 || { echo 'ignore error'; }

EOF
)"
    _runAsRoot "${cmd}"
}

global(){
    cat<<EOF
Tips:
    Step 1. set rule from Rule to Global in config file,then restart service
    Step 2. clash.sh setAnyProxy <configFilename> GLOBAL <proxy_name|proxy_group_name,usally use Nexitally>
EOF
}

_clear(){
    echo "_clear"
    cmd="$(cat<<EOF
    iptables -t mangle -D PREROUTING -j clash
    iptables -t mangle -F clash
    iptables -t mangle -X clash

    ip rule del fwmark 1 table 100
EOF
)"
    _runAsRoot "${cmd}"
}

config(){
    local name=${1:?'missing config name(no extension)'}
    name=${name%.json}
    local subDir="${root}/etc/${name}"

    if [ ! -e ${subDir} ];then
        echo "Error: not found runtime dir: ${subDir}"
        exit 1
    fi
    local configFile="${subDir}/${name}.yaml"
    mtime0="$(${cmdStat} $configFile | grep Modify)"
    $ed $configFile
    mtime1="$(${cmdStat} $configFile | grep Modify)"
    #配置文件被修改
    if [ "$mtime0" != "$mtime1" ];then
        #并且当前是运行状态，则重启服务
        if status ${name} >/dev/null;then
            echo "Config file changed,restart server"
            restart ${name}
        fi
    fi
}


setAnyProxy(){
    local name=${1:?'missing config name(no extension)'}
    name=${name%.json}
    local subDir="${root}/etc/${name}"

    if [ ! -e ${subDir} ];then
        echo "Error: not found runtime dir: ${subDir}"
        exit 1
    fi
    local configFile="${subDir}/${name}.yaml"

    local src=${2:?'missing source proxy'}
    local dst=${3:?'missing destination proxy'}


    local controllerHost="$(perl -lne 'print $1 if /external-controller:\s+([\d:.]+)/' ${configFile}|tr -d ' ')"
    echo "controllerHost: ${controllerHost}"
    curl -XPUT -d "{\"name\":\"${dst}\"}" http://${controllerHost}/proxies/${src}

}

switchProxy(){
    setAnyProxy "$1" "Nexitally" "$2"
}

listAnyProxy(){
    local name=${1:?'missing config name(no extension)'}
    name=${name%.json}
    local subDir="${root}/etc/${name}"

    if [ ! -e ${subDir} ];then
        echo "Error: not found runtime dir: ${subDir}"
        exit 1
    fi
    local configFile="${subDir}/${name}.yaml"
    local src=${2:?'missing group name'}

    local controllerHost="$(perl -lne 'print $1 if /external-controller:\s+([\d:.]+)/' ${configFile}|tr -d ' ')"
    local fullURL="http://$controllerHost/proxies/${src}"
    if command -v python >/dev/null 2>&1;then
        curl -x "" -s "${fullURL}" | python -m json.tool
    else
        curl -x "" "${fullURL}"
    fi

}

listProxy(){
    local name=${1:?'missing config name(no extension)'}
    listAnyProxy $name Nexitally
}

ipinfo(){
    local name=${1:?'missing config name(no extension)'}
    name=${name%.json}
    local subDir="${root}/etc/${name}"

    if [ ! -e ${subDir} ];then
        echo "Error: not found runtime dir: ${subDir}"
        exit 1
    fi
    local configFile="${subDir}/${name}.yaml"
    local socksPort=$(grep '^socks-port:' "${configFile}" 2>/dev/null | awk '{print $2}' | grep -o [0-9][0-9]*)

    echo ">> ip info from ipinfo.io:"
    curl -m 5 -x socks5://localhost:${socksPort} ipinfo.io
    echo
    echo ">> ip info from myip.ipip.net"
    curl -m 5 -x socks5://localhost:${socksPort} myip.ipip.net
}

restart(){
    local name=${1:?'missing config name(no extension)'}
    name=${name%.json}
    local subDir="${root}/etc/${name}"

    if [ ! -e ${subDir} ];then
        echo "Error: not found runtime dir: ${subDir}"
        exit 1
    fi
    stop ${name}
    start ${name}
}

list(){
    (cd ${etcDir} && find . -iname "*.yaml")
}

status(){
    local name=${1:?'missing config name(no extension)'}
    name=${name%.json}
    local subDir="${root}/etc/${name}"

    if [ ! -e ${subDir} ];then
        echo "Error: not found runtime dir: ${subDir}"
        exit 1
    fi
    local configFile="${subDir}/${name}.yaml"

    if ! ps aux | grep clash | grep -v grep | grep -q "${name}.yaml"; then
        echo "$name not running"
        return 1
    fi
    local port=$(grep '^port:' "${configFile}" 2>/dev/null | awk '{print $2}')
    echo "clash is running on port: $port"
    if curl -x http://localhost:$port ifconfig.me >/dev/null 2>&1; then
        echo "Working on port: ${port}"
    else
        echo "${name} is running,but not work"
    fi
}

log(){
    local name=${1:?'missing config name(no extension)'}
    name=${name%.json}
    local subDir="${root}/etc/${name}"

    if [ ! -e ${subDir} ];then
        echo "Error: not found runtime dir: ${subDir}"
        exit 1
    fi

    case $(uname) in
        Linux)
            sudo journalctl -u clash-${name}.service -f
            ;;
        Darwin)
            echo "Watching ${name}.log..."
            tail -f /tmp/${name}.log
            ;;
    esac
}

em(){
    $ed $0
}

###############################################################################
# write your code above
###############################################################################
function _help(){
    cd ${this}
    cat<<EOF2
Usage: $(basename $0) ${bold}CMD${reset}

${bold}CMD${reset}:
EOF2
    perl -lne 'print "\t$2" if /^\s*(function)?\s*(\S+)\s*\(\)\s*\{$/' $(basename ${BASH_SOURCE}) | perl -lne "print if /^\t[^_]/"
}

case "$1" in
     ""|-h|--help|help)
        _help
        ;;
    *)
        "$@"
esac
